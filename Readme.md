# Modul 231
### Inhaltverzeichnis 
* [-Pinguine](Aufgaben/Pinguine.md)  
* [-CIA](Aufgaben/CIA.md)  
* [-Das Internet als Datenarchiv](Aufgaben/Das%20Internet%20als%20Datenarchiv.md)
* [-Authentifizierung](Aufgaben/Authentifizierung.md)
* [-Passwort hacken](Aufgaben/Passworthacken.md)
* [-Backup](Aufgaben/Backup.md)
* [-Passwortmanager](Aufgaben/Passwortmanager.md)
* [-Verschlüsselung](Aufgaben/Verschlüsselung.md)