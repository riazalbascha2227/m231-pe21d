## Passwortmanager

Ein Passwort-Manager ist ein Programm oder eine Anwendung, die es ermöglicht, Passwörter sicher zu speichern und automatisch einzufügen, wenn sie auf einer Website oder in einer Anwendung benötigt werden. Dies ermöglicht es Benutzern, sichere und einzigartige Passwörter für jeden Online-Account zu verwenden, ohne sich jedes Passwort merken zu müssen.

### Bewertungen

1. NordPass 4.9/5
2. RoboForm 4.6/5
3. Keeper   4.4/5

### Beispiele

#### Google Passwort Manager:

Der Google Passwort Manager hat einen Passwort Check bei dem man sehen kann wie viele Passwörter, die gespeichert wurden, wiederverwendet wurden und welche schwache Passwörter sind. Man kann sehen auf welchen Webseiten die Passwörter gespeichert wurden und man kann gezielt nach Passwörtern suchen.

##### Einstellungen
![Einstellungen](../Bilder/Einstellungen.png)
##### On-Device-Verschlüsselung
 Für zusätzliche Sicherheit kann man seine Passwörter zuerst verschlüsseln, bevor sie im Google Passwort Manager gespeichert werden.

 #### Lass Pass
 Mit dem Add on Lass Pass lassen sich Passwörter von verschiedenen Webseiten genauer definieren. Man fügt den Username und dass Passwort hinzu und kann verschiedene Ordner auswählen in denen diese gespeichert werden.
 ##### Einstellungen
 ![Lass Pass](../Bilder/Einstellungen2.png)

 ### Vergleich
||Google Password Manager|Lass Pass|
|--------|-----------------------|---------|
|On-Device-Verschlüsselung|Ja|Nein|
|Automatisches anmelden|Nein|Ja|
|Passwörter exportieren|Ja|Nein|
|Passwörter suchen|Ja|Nein|

## Reflexion

Diese Aufgabe fand ich sehr leicht, da man einfach nur recherchieren musste. Nur bei den Bewertungen gab es viele verschiedene Versionen. Ich habe mich einfach für eine dieser Entschieden.
